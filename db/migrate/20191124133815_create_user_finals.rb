class CreateUserFinals < ActiveRecord::Migration[6.0]
  def change
    create_table :user_finals do |t|
      t.string :username
      t.string :password_digest
      t.string :name
      t.string :surname
      t.string :address
      t.string :email
      t.string :telephone

      t.timestamps
    end
    add_index :user_finals, :username, unique: true
  end
end
