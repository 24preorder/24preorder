require 'test_helper'

class MyUsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @my_user = my_users(:one)
  end

  test "should get index" do
    get my_users_url
    assert_response :success
  end

  test "should get new" do
    get new_my_user_url
    assert_response :success
  end

  test "should create my_user" do
    assert_difference('MyUser.count') do
      post my_users_url, params: { my_user: { email: @my_user.email, password: 'secret', password_confirmation: 'secret' } }
    end

    assert_redirected_to my_user_url(MyUser.last)
  end

  test "should show my_user" do
    get my_user_url(@my_user)
    assert_response :success
  end

  test "should get edit" do
    get edit_my_user_url(@my_user)
    assert_response :success
  end

  test "should update my_user" do
    patch my_user_url(@my_user), params: { my_user: { email: @my_user.email, password: 'secret', password_confirmation: 'secret' } }
    assert_redirected_to my_user_url(@my_user)
  end

  test "should destroy my_user" do
    assert_difference('MyUser.count', -1) do
      delete my_user_url(@my_user)
    end

    assert_redirected_to my_users_url
  end
end
