require 'rails_helper'

describe UsersController,type: :request do 
    describe 'signup' do
        it 'check view signup in controller' do
            get signup_path
            expect(response).to render_template('users/new')
        end
    end
end
