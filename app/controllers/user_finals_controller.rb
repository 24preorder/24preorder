class UserFinalsController < ApplicationController
  before_action :set_user_final, only: [:show, :edit, :update, :destroy]

  # GET /user_finals
  # GET /user_finals.json
  def index
    @user_finals = UserFinal.all
  end

  # GET /user_finals/1
  # GET /user_finals/1.json
  def show
  end

  # GET /user_finals/new
  def new
    @user_final = UserFinal.new
  end

  # GET /user_finals/1/edit
  def edit
  end

  # POST /user_finals
  # POST /user_finals.json
  def create
    @user_final = UserFinal.new(user_final_params)

    respond_to do |format|
      if @user_final.save
        format.html { redirect_to @user_final, notice: 'User final was successfully created.' }
        format.json { render :show, status: :created, location: @user_final }
      else
        format.html { render :new }
        format.json { render json: @user_final.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /user_finals/1
  # PATCH/PUT /user_finals/1.json
  def update
    respond_to do |format|
      if @user_final.update(user_final_params)
        format.html { redirect_to @user_final, notice: 'User final was successfully updated.' }
        format.json { render :show, status: :ok, location: @user_final }
      else
        format.html { render :edit }
        format.json { render json: @user_final.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /user_finals/1
  # DELETE /user_finals/1.json
  def destroy
    @user_final.destroy
    respond_to do |format|
      format.html { redirect_to user_finals_url, notice: 'User final was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_final
      @user_final = UserFinal.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_final_params
      params.require(:user_final).permit(:username, :password, :password_confirmation, :name, :surname, :address, :email, :telephone)
    end
end
